<?php

	namespace Src;

	use \PDO;
	use \PDOException;

	class Database
	{

		private ?PDO $_dbConnection = null;

		/**
		 * Database constructor.
		 */
		public function __construct()
		{
			$db = getenv('DB_DATABASE');
			$user = getenv('DB_USERNAME');
			$passwd = getenv('DB_PASSWORD');
			$host = getenv('DB_HOST');
			$port = getenv('DB_PORT');

			try {
				$this->_dbConnection = new PDO(
					"mysql:host=$host;port=$port;charset=utf8mb4;dbname=$db",
					$user,
					$passwd
				);
				$this->_dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}

		/**
		 * @return PDO|null
		 */
		public function getConnection(): ?PDO
		{
			return $this->_dbConnection;
		}

		/**
		 * A very ugly method for PDO multiple rows upsert, does it job here but not for production usage :)
		 * It's better to use some kind of a library for that
		 *
		 * @param $tblName
		 * @param $colNames
		 * @param $dataValues
		 */
		public function upsertMultipleRows($tblName, $colNames, $dataValues): void
		{
			try {
				$dataToInsert = [];
				foreach ($dataValues as $data) {
					foreach ($data as $val) {
						$dataToInsert[] = $val;
					}
				}

				$updateCols = [];
				foreach ($colNames as $curCol) {
					$updateCols[] = $curCol . " = VALUES($curCol)";
				}
				$onDup = implode(', ', $updateCols);
				$allPlaces = implode(',', array_fill(0, count($dataValues), '(' .
					str_pad('', (count($colNames) * 2) - 1, '?,') . ')'));

				$sql = "INSERT INTO $tblName (" . implode(', ', $colNames) .
					") VALUES " . $allPlaces . " ON DUPLICATE KEY UPDATE $onDup";

				$statement = $this->_dbConnection->prepare($sql);
				$statement->execute($dataToInsert);
			} catch (PDOException $e) {
				exit($e->getMessage());
			}
		}

		/**
		 * @param $sql
		 * @return array
		 */
		public function fetchAllRecordsBySql($sql): array
		{
			try {
				$statement = $this->_dbConnection->query($sql);
				return $statement->fetchAll(PDO::FETCH_ASSOC);
			} catch (PDOException $e) {
				exit($e->getMessage());
			}
		}

		/**
		 * @param $sql
		 * @return array|bool
		 */
		public function fetchRecordBySql($sql)
		{
			try {
				$statement = $this->_dbConnection->query($sql);
				return $statement->fetch(PDO::FETCH_ASSOC);
			} catch (PDOException $e) {
				exit($e->getMessage());
			}
		}

		/**
		 * @param $token
		 * @return int
		 */
		public function storeTokenInDB($token): int
		{
			try {
				//just a workaround to store one token
				$statement = $this->_dbConnection->prepare('TRUNCATE TABLE token');
				$statement->execute();

				$statement = $this->_dbConnection->prepare("INSERT INTO TOKEN (token) VALUES (:token)");
				$statement->execute([':token' => $token]);

				return $statement->rowCount();
			} catch (PDOException $e) {
				exit($e->getMessage());
			}
		}
	}
