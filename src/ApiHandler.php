<?php

	namespace Src;

	use \Exception;

	class ApiHandler
	{

		private Database $db;
		private string $_clientId;
		private string $_authEmail;
		private string $_authName;
		private string $_apiUrl;

		/**
		 * ApiHandler constructor.
		 */
		public function __construct($db)
		{
			$this->db = $db;
			$this->_clientId = getenv('CLIENT_ID');
			$this->_authEmail = getenv('AUTH_EMAIL');
			$this->_authName = getenv('AUTH_NAME');
			$this->_apiUrl = getenv('API_URL');
		}

		/**
		 * Access token can be stored in a cache but in this case I will use the database
		 * to avoid installing additional cache packages.
		 *
		 * @throws Exception
		 */
		public function fetchAccessTokenAPI(): string
		{
			$options = http_build_query([
				'client_id' => $this->_clientId,
				'email' => $this->_authEmail,
				'name' => $this->_authName,
			]);
			$response = $this->_curl('register', $options, true);

			if (!is_array($response) || isset($response['error'])) {
				throw new Exception('Failed to fetch auth token');
			}

			$token = $response['data']['sl_token'];
			$this->db->storeTokenInDB($token);
			return $token;
		}

		/**
		 * @return array|bool
		 */
		public function fetchAccessTokenLocally()
		{
			return $this->db->fetchRecordBySql(
				'SELECT `token` FROM `token` WHERE `created_time` > DATE_SUB(NOW(),INTERVAL 1 HOUR)'
			);
		}

		/**
		 * @throws Exception
		 */
		public function getAccessToken(): string
		{
			$savedToken = $this->fetchAccessTokenLocally();
			return !$savedToken
				? $this->fetchAccessTokenAPI()
				: $savedToken['token'];
		}

		/**
		 * @param int $page
		 * @return array
		 * @throws Exception
		 */
		public function getPosts($page = 1): array
		{
			$options = http_build_query([
				'sl_token' => $this->getAccessToken(),
				'page' => $page,
			]);

			$response = $this->_curl('posts?' . $options);

			if (!is_array($response) || isset($response['error']) || !isset($response['data']['posts'])) {
				throw new Exception('Failed to fetch posts');
			}
			return $response;
		}

		/**
		 * @param $uri
		 * @param null $options
		 * @param false $isPost
		 * @return array
		 */
		private function _curl($uri, $options = null, $isPost = false): array
		{
			try {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $this->_apiUrl . $uri);
				if (!is_null($options)) {
					curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
				}
				if ($isPost) {
					curl_setopt($ch, CURLOPT_POST, true);
				}
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = curl_exec($ch);
				curl_close($ch);
			} catch (Exception $exception) {
				die('Curl request failed with error: ' . $exception->getMessage());
			}
			return json_decode($output, true);
		}
	}
