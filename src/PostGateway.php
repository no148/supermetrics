<?php

	namespace Src;

	use Carbon\Carbon;
	use Exception;

	class PostGateway
	{
		private Database $db;

		/**
		 * PostGateway constructor.
		 * @param $db
		 */
		public function __construct($db)
		{
			$this->db = $db;
		}

		/**
		 * It might make sense to synchronize posts only once in a period of time
		 * if there would be too many calls to statistics display or cache the result of statistics calculation.
		 * Also we could update posts only if there were some changes if we had update_time field.
		 *
		 * @param ApiHandler $apiHandler
		 * @throws Exception
		 */
		public function synchronizePosts(ApiHandler $apiHandler): void
		{
			$page = 0;
			do {
				$page++;
				$postsResponse = $apiHandler->getPosts($page);
				$this->upsertPosts($postsResponse['data']['posts']);
				$responsePage = (int)$postsResponse['data']['page'];
			} while ($responsePage === $page);
		}

		/**
		 * @param $posts
		 */
		public function upsertPosts($posts): void
		{
			$dataValues = [];
			$colNames = array_keys($posts[0]);
			foreach ($posts as $post) {
				$post['created_time'] = Carbon::createFromTimeString($post['created_time'])->toDateTimeString();
				$dataValues[] = array_values($post);
			}
			$this->db->upsertMultipleRows('posts', $colNames, $dataValues);
		}

		/**
		 * Can be cached response if there are too many requests for statistics.
		 *
		 * @return false|string
		 */
		public function displayStatistics()
		{
			return json_encode([
				'averagePostLengthMonthly' => $this->formatStatistics($this->getAveragePostLengthMonthly()),
				'longestPostMonthly' => $this->formatStatistics($this->getLongestPostMonthly()),
				'getPostsCountWeekly' => $this->formatStatistics($this->getPostsCountWeekly()),
				'averagePostsPerUserMonthly' => $this->formatStatistics($this->getAveragePostsPerUserMonthly()),
			], JSON_PRETTY_PRINT);
		}

		/**
		 * The keys of response are formatted in human readable way for reading convenience
		 * For real server to server interaction the keys must be in digit only format
		 *
		 * @param $dataSet
		 * @return array
		 */
		public function formatStatistics($dataSet): array
		{
			$formattedSet = [];
			foreach ($dataSet as $dataPortion) {
				$formattedSet[$dataPortion['period']] = $dataPortion['value'];
			}
			return $formattedSet;
		}

		/**
		 * @return array
		 */
		public function getAveragePostLengthMonthly(): array
		{
			$sql = "
				SELECT CONCAT(MONTHNAME(`created_time`), ' ', YEAR(`created_time`)) as `period`,
					   avg(length(`message`)) AS `value` 
				FROM `posts` 
				GROUP BY MONTH(`created_time`) 
				ORDER BY `created_time`
				DESC;
			";
			return $this->db->fetchAllRecordsBySql($sql);
		}

		/**
		 * @return array
		 */
		public function getLongestPostMonthly(): array
		{
			$sql = "
				SELECT CONCAT(MONTHNAME(`created_time`), ' ', YEAR(`created_time`)) as `period`,
					   MAX(length(`message`)) as `value`
				FROM `posts` 
				GROUP BY MONTH(`created_time`) 
				ORDER BY `created_time`
				DESC;
			";
			return $this->db->fetchAllRecordsBySql($sql);
		}

		/**
		 * @return array
		 */
		public function getAveragePostsPerUserMonthly(): array
		{
			$sql = "
				SELECT CONCAT(MONTHNAME(`created_time`), ' ', YEAR(`created_time`)) as `period`, `avg_post_count`/`users_count` as `value` FROM
					 ( SELECT count(`message`) AS `avg_post_count`, `created_time`
					   FROM `posts` 
					   GROUP BY MONTH(`created_time`) 
					   ORDER BY `created_time`
					   DESC ) AS a,
					 (	SELECT DISTINCT count(DISTINCT `from_id`) AS `users_count` 
					   FROM `posts`
					   GROUP BY MONTH(`created_time`)
					   ORDER BY `created_time`
					   DESC ) AS b;
			";
			return $this->db->fetchAllRecordsBySql($sql);
		}

		/**
		 * @return array
		 */
		public function getPostsCountWeekly(): array
		{
			$sql = "
				SELECT CONCAT(WEEK(`created_time`, 3), ' week ', YEAR(`created_time`)) as `period`,
					   count(`message`) AS `value` 
				FROM `posts` 
				GROUP BY WEEK(`created_time`, 3) 
				ORDER BY `created_time`
				DESC;
			";
			return $this->db->fetchAllRecordsBySql($sql);
		}
	}