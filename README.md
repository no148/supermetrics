# README #

Welcome to supermetrics test task.  
I've decided to build this demo application for terminal usage only since it's supposed to comunicate server to server.  
Considering the data level from the providing API it make sence to use MYSQL database and store it for statistics calculation.  
I believe the fastest way to calculate desired statistics values is to do it via sql queries.  

To sum up the app flow is as follow:

* Grab api access token and store it in the database, after it expires we get a new one and refresh it
* Grab posts using acces token via api call and store it in the database
* Calculate, format and display the desired statistics in the terminal

### What is this repository for? ###

* Fetch and manipulate JSON data from a fictional Supermetrics Social Network REST API

### Used packages: ###

* *nesbot/carbon* for data and time formatting and manipulation
* *vlucas/phpdotenv* for .env support and credentials storage

### How do I get set up? ###

* Run composer install
* Copy .env.example to .env and populate database credentials
* Run php db_migration.php from the command line
* Run php index.php
