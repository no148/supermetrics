<?php

	require 'vendor/autoload.php';

	use Dotenv\Dotenv;
	use Src\Database;

	$env = new DotEnv(__DIR__);
	$env->load();

	$db = new Database();
