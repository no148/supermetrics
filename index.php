<?php

	require "bootstrap.php";

	use Src\ApiHandler;
	use Src\PostGateway;

	try {
		$apiHandler = new ApiHandler($db);
		$postGateway = new PostGateway($db);

		$postGateway->synchronizePosts($apiHandler);
		echo $postGateway->displayStatistics() . PHP_EOL;
	} catch (Exception $exception) {
		die($exception->getMessage());
	}