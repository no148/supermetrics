<?php

	require 'bootstrap.php';

	$sqlPosts = "CREATE TABLE IF NOT EXISTS posts (
		`id` VARCHAR(100) NOT NULL,
		`from_name` VARCHAR(100) NOT NULL,
		`from_id` VARCHAR(100) NOT NULL,
		`message` TEXT DEFAULT NULL,
		`type` VARCHAR(100) NOT NULL,
		`created_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (id)
	)";

	$sqlToken = "CREATE TABLE IF NOT EXISTS token (
		`id` INT NOT NULL AUTO_INCREMENT,
		`token` VARCHAR(100) NOT NULL,
		`created_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (id)
	)";

	try {
		$db->getConnection()->exec($sqlPosts);
		$db->getConnection()->exec($sqlToken);
		echo "Successfully migrated!\n";
	} catch (PDOException $e) {
		exit($e->getMessage());
	}